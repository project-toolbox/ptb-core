<?php
/**
 * Project Toolbox - ptbDatabases
 * Configuration of available Databases
 *
 * @copyright coreweb GmbH
 * @author Christoph Vieth <cvieth@coreweb.de>
 * @version 1.0
 */
require_once(ptbCoreConfig::pathBase . ptbCoreConfig::pathLibraries . 'ptbDbConfig.php');

/**
 * @var $ptbDatabases Array
 */
$ptbDatabases = array();

/**
 * Example Database Configuration
 */
$exampleDb = new ptbDbConfig();
$exampleDb->setDbName('example');
$exampleDb->setDbHost('localhost');
$exampleDb->setDbPort(3306);
$exampleDb->setDbUser('username');
$exampleDb->getDbPass('password');
$exampleDb->setDbSchema('example_database');
array_push($ptbDatabases, $exampleDb);
unset($exampleDb);


