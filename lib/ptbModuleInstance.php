<?php
/**
 * Project Toolbox - ptbModuleInstance
 *
 * Interface for Project Toolbox Modules
 *
 * @copyright 2013 - coreweb GmbH
 * @author Christoph Vieth <cvieth@coreweb.de>
 * @version 1.1
 */
interface ptbModuleInstance
{
    /**
     * Executes the module
     * @return void
     */
    public function run();
}