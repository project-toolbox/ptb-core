<?php
/**
 * Project Toolbox - ptbMailSender
 *
 * Library for sending Emails
 *
 * @copyright coreweb GmbH
 * @author Christoph Vieth <cvieth@coreweb.de>
 * @version 1.1
 */

// Load Configuration
require_once(ptbCoreConfig::pathBase . ptbCoreConfig::pathConfigs . 'ptbMailConfig.php');

// Load ptbDbObject if required
if (ptbMailConfig::logEnabled) {
    require_once(ptbCoreConfig::pathBase . ptbCoreConfig::pathLibraries . 'ptbDbObject.php');
}


class ptbMailSender
{
    const eol = "\r\n";
    const mimeVersion = 'MIME-Version: 1.0';
    const contentTypeHTML = 'text/html; charset=utf-8';
    const contentTypePlain = 'text/plain; charset=utf-8';
    const contentTypeMultipart = 'multipart/alternative';
    const contentTransEnc = 'utf8';
    private $sender = '';
    private $receiver = null;
    private $subject = '';
    private $content = array();
    private $messageId = null;
    private $mimeBoundary = '';
    private $contentList = array();
    private $contentCount = 0;
    private $contentLastId = null;

    /**
     * Constructor of Library
     */
    public function __construct()
    {
        // Set default sender
        $this->sender = ptbMailConfig::defaultSender;

        // Create MIME Boundary if required
        if (ptbMailConfig::multipartEnabled)
            $this->mimeBoundary = $this->createMimeBoundary();
    }

    /**
     * Create MIME Boundary
     *
     * @return string
     */
    private function createMimeBoundary()
    {
        $random = md5(time() . microtime());
        return $this->mimeBoundary = 'MULTIPART_BOUNDARY_' . $random;
    }

    /**
     * Set email receiver
     *
     * @param string $address
     * @param string $name
     * @return string
     * @throws Exception
     */
    public function setReceiver($address, $name = null)
    {
        if (filter_var($address, FILTER_VALIDATE_EMAIL)) {
            if (is_null($name) || (strlen($name) <= 0)) {
                return $this->receiver = $address;
            } else {
                return $this->receiver = $name . ' <' . $address . '>';
            }
        } else throw new Exception(LANG_ERROR_MAIL_RECEIVER_INVALID);
    }

    /**
     * Set Mail sender
     *
     * @param string $address
     * @param string $name
     * @return string
     * @throws Exception
     */
    public function setSender($address, $name = null)
    {
        if (filter_var($address, FILTER_VALIDATE_EMAIL)) {
            if (is_null($name) || (strlen($name) <= 0)) {
                return $this->sender = $address;
            } else {
                return $this->sender = $name . ' <' . $address . '>';
            }
        } else throw new Exception(LANG_ERROR_MAIL_SENDER_INVALID);
    }

    public function setSubject($subject)
    {
        return $this->subject = $subject;
    }

    /**
     * Add email content to content list
     *
     * @param string $content
     * @param string $type
     * @return bool
     * @throws Exception
     */
    public function addContent($content, $type = 'plain')
    {
        if ((ptbMailConfig::multipartEnabled == false) && ($this->contentCount > 1)) {
            throw new Exception(LANG_ERROR_MAIL_MULTIPART_DISABLED);
        }

        switch ($type) {
            case 'plain':
                $contentNew = 'Content-Type: ' . self::contentTypePlain . self::eol;
                $contentNew .= 'Content-Transfer-Encoding: ' . self::contentTransEnc . self::eol . self::eol;
                $contentNew .= $content;
                break;

            case 'html':
                $contentNew = 'Content-Type: ' . self::contentTypeHTML . self::eol;
                $contentNew .= 'Content-Transfer-Encoding: ' . self::contentTransEnc . self::eol . self::eol;
                $contentNew .= $content;
                break;

            default:
                throw new Exception(LANG_ERROR_MAIL_UNKNOWN_CONTENT_TYPE);
                break;
        }

        $contentId = array_push($this->contentList, $contentNew);

        if ($contentId != false) {
            $this->contentLastId = $contentId;
            $this->contentCount++;
            return true;
        } else {
            return false;
        }

    }

    /**
     * Send prepared mail
     *
     * @throws Exception
     */
    public function send()
    {
        if (is_null($this->receiver)) {
            throw new Exception(LANG_ERROR_MAIL_NO_RECIVER);
        }

        if (count($this->content) == 0) {
            throw new Exception(LANG_ERROR_MAIL_NO_CONTENT);
        }

        $header = '';
        $header .= self::mimeVersion . self::eol;
        $header .= 'From: ' . $this->sender . self::eol;
        $header .= 'Message-Id: <' . $this->getMessageId() . '@' . ptbMailConfig::domainMessageId . '>' . self::eol;
        if (ptbMailConfig::multipartEnabled) {
            $header .= 'Content-Type: ' . self::contentTypeMultipart . '; boundary=' . $this->mimeBoundary . self::eol;

            $body = LANG_MAIL_MIME_NOTICE . self::eol;

            foreach ($this->content as $content) {
                $body .= '--' . $this->mimeBoundary . self::eol;
                $body .= $content . self::eol;
            }

            $body .= '--' . $this->mimeBoundary . '--' . self::eol;
        } else {
            $content = $this->content;
            $content = explode(self::eol, $content[0]);
            $header .= $content[0] . self::eol;
            $header .= $content[1] . self::eol;
            $body = $content[2] . self::eol;
        }
        if (mail($this->receiver, $this->subject, $body, $header))
            if (ptbMailConfig::logEnabled)
                $this->logMail();
    }

    /**
     * Get the message id
     *
     * @return string
     */
    public function getMessageId()
    {
        return is_null($this->messageId) ? $this->createMessageId() : $this->messageId;
    }

    /**
     * Set a message id
     *
     * @param $id
     * @return mixed
     */
    public function setMessageId($id)
    {
        return $this->messageId = $id;
    }

    /**
     * Create a unique message id
     *
     * @return string
     */
    private function createMessageId()
    {
        return $this->messageId = md5($this->receiver . utf8_decode($this->subject) . microtime());
    }

    /**
     * Write a log entry to log database
     *
     * @return bool
     */
    private function logMail()
    {
        if (ptbMailConfig::logEnabled) {
            global $ptbDbObject;
            $database = $ptbDbObject->getConnection(ptbMailConfig::logDbConfig);

            if (get_class($database) == PDO) {

                // Prepare query
                $logDb = $database->prepare("INSERT INTO " . ptbMailConfig::logDbTable . " (`reciver`, `msg_id`, `subject`,`message`)
                                         VALUES (:reciver, :msg_id, :subject, :message);");

                // Bind parameters
                $logDb->bindParam('reciver', $this->receiver);
                $logDb->bindParam('msg_id', $this->messageId);
                $logDb->bindParam('subject', $this->subject);
                $logDb->bindParam('message', $this->content[0]);

                // Execute query
                return $logDb->execute();
            } else {
                return false;
            }
        }
    }

}