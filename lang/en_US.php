<?php
/**
 * Project Toolbox - en_US
 *
 * Language file for en_US locale.
 *
 * @copyright coreweb GmbH
 * @author Christoph Vieth <cvieth@coreweb.de>
 * @version 1.0
 */

/*
 * Module Loader
 */
define('LANG_ERROR_MODULE_ALREADY_LOADED', 'The given module is already loaded.');
define('LANG_ERROR_MODULE_NOT_LOADED', 'The given module is not loaded.');

/*
 * Database Class
 */
define('LANG_ERROR_DB_CONF_NOT_ARRAY', 'Given database configuration list is not an array.');
define('LANG_ERROR_DB_CONF_NOT_OBJECT', "Given database configuration is is no 'ptbDbConfig' object.");
define('LANG_ERROR_DB_MESSAGE', 'Database Error: ');

/*
 * Mail Sender Class
 */
define('LANG_ERROR_MAIL_NO_RECIVER', 'Mail reciver not set.');
define('LANG_ERROR_MAIL_NO_CONTENT', 'Mail content not set.');
define('LANG_ERROR_MAIL_UNKNOWN_CONTENT_TYPE', 'Given content type is unknown.');
define('LANG_ERROR_MAIL_SENDER_INVALID', 'Given sender address is invalid.');
define('LANG_ERROR_MAIL_RECEIVER_INVALID', 'Given receiver address is invalid.');
define('LANG_ERROR_MAIL_MULTIPART_DISABLED' , 'Multipart mailing is disabled. Only one Content element is allowed.');
define('LANG_MAIL_MIME_NOTICE', 'This is a multipart message in MIME format.');