<?php
/**
 * Project Toolbox - ptbHttpHeader
 *
 * Simplifier class for HTTP headers
 *
 * @copyright 2013 - coreweb GmbH
 * @author Christoph Vieth <cvieth@coreweb.de>
 * @version 1.1
 */

class ptbHttpHeader
{
    const ENCODING_UTF8 = 'Content-Encoding: UTF-8';
    const STATUS_200 = 'HTTP/1.1 200 OK';
    const STATUS_400 = 'HTTP/1.1 400 Bad Request';
    const STATUS_401 = 'HTTP/1.1 401 Unauthorized';
    const STATUS_403 = 'HTTP/1.1 403 Forbidden';
    const STATUS_404 = 'HTTP/1.1 404 Not Found';
    const STATUS_405 = 'HTTP/1.1 405 Method Not Allowed';
    const CONTENT_TYPE = 'Content-type: ';
    const CONTENT_TYPE_HTML = 'Content-type: text/html; charset=UTF-8';
    const CONTENT_TYPE_JSON = 'Content-type: application/json; charset=UTF-8';
    const CONTENT_TYPE_JS = 'Content-type: text/javascript; charset=UTF-8';
    const EXPIRES = 'Expires: Thu, 03 Apr 1986 05:00:00 GMT';
    const CACHE_CONTROL = 'Cache-Control: no-cache, no-store, max-age=0, must-revalidate';
}