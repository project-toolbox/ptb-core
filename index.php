<?php
/**
 * Project Toolbox - ptb-base-skeleton
 *
 * @copyright coreweb GmbH
 * @author Christoph Vieth <cvieth@coreweb.de>
 */

/**
 * Display all error messages
 */
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);



/**
 * Load base configuration
 */
require_once('conf/ptbCoreConfig.php');

/**
 * Load core language file
 */
require_once(ptbCoreConfig::pathBase . ptbCoreConfig::pathLanguage . ptbCoreConfig::localeDefault . '.php');


/**
 * Loading modules
 *
 * @todo: Load ony executed modules into memory
 */
require_once ptbCoreConfig::pathLibraries . 'ptbModuleLoader.php';

$loader = new ptbModuleLoader(ptbCoreConfig::pathModules);

if (isset($_REQUEST['module'])) {
    if ($loader->exists($_REQUEST['module'])) {
        $loader->run($_REQUEST['module']);
    }
} else {
   //header(ptbHttpHeader::STATUS_404);
    echo "Module not set!";
}