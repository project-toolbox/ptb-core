<?php
class ptbMailConfig
{
    const logEnabled = true;
    const logDbConfig = 'example';
    const logDbTable = '`project_mail_log`';

    // Domain for Message ID
    const domainMessageId = 'example.com';

    const multipartEnabled = true;

    const defaultSender = 'Example Sender <sender@example.com>';
}