<?php
/**
 * Project Toolbox - ptbBaseConfig
 * Base configuration of "Project Toolbox"
 *
 * @copyright coreweb GmbH
 * @author Christoph Vieth <cvieth@coreweb.de>
 * @version 1.2
 */

class ptbCoreConfig {
    // Set default locale
    const localeDefault = 'en_US';

    // Setting Path Configurations
    const pathBase = '/var/www/ptbInstance/';
    const pathLanguage = 'lang/';
    const pathConfigs = 'conf/';
    const pathModules = 'mdl/';
    const pathLibraries = 'lib/';
}
