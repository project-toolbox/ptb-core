<?php
/**
 * Project Toolbox - ptbDbConfig
 *
 * Library for Configuration of a database connection for ptbDbObject.
 *
 * @copyright coreweb GmbH
 * @author Christoph Vieth <cvieth@coreweb.de>
 * @version 1.3
 */

class ptbDbConfig
{
    const defaultPort = 3306;
    private $dbName = null;
    private $dbUser = null;
    private $dbPass = null;
    private $dbHost = null;
    private $dbPort = null;
    private $dbSchema = null;

    /**
     * Get name of database connection
     *
     * @return string
     */
    public function getDbName()
    {
        return $this->dbName;
    }

    /**
     * Set name of database connection
     *
     * @param string $dbName
     */
    public function setDbName($dbName)
    {
        $this->dbName = $dbName;
    }

    /**
     * Get host of database connection
     *
     * @return string
     */
    public function getDbHost()
    {
        return $this->dbHost;
    }

    /**
     * Set host of database connection
     *
     * @param string $dbHost
     */
    public function setDbHost($dbHost)
    {
        $this->dbHost = $dbHost;
    }

    /**
     * Get password of database connection
     *
     * @return string
     */
    public function getDbPass()
    {
        return $this->dbPass;
    }

    /**
     * Set password of database connection
     *
     * @param string $dbPass
     */
    public function setDbPass($dbPass)
    {
        $this->dbPass = $dbPass;
    }

    /**
     * Get port of database connection
     *
     * @return int
     */
    public function getDbPort()
    {
        if (is_null($this->dbPort)) {
            return self::defaultPort;
        } else {
            return $this->dbPort;
        }
    }

    /**
     * Set port of database connection
     *
     * @param int $dbPort
     * @return bool|int
     */
    public function setDbPort($dbPort)
    {
        if (is_numeric($dbPort)) {
            return $this->dbPort = $dbPort;
        } else {
            return false;
        }
    }

    /**
     * Get schema of database connection
     *
     * @return string
     */
    public function getDbSchema()
    {
        return $this->dbSchema;
    }

    /**
     * Set schema of database connection
     *
     * @param string $dbSchema
     */
    public function setDbSchema($dbSchema)
    {
        $this->dbSchema = $dbSchema;
    }

    /**
     * Get user of database connection
     *
     * @return string
     */
    public function getDbUser()
    {
        return $this->dbUser;
    }

    /**
     * Set user of database connection
     *
     * @param string $dbUser
     */
    public function setDbUser($dbUser)
    {
        $this->dbUser = $dbUser;
    }

}