<?php
/**
 * Project Toolbox - ptbModuleLoader
 *
 * Library to load and manage Project Toolbox modules
 *
 * @copyright 2013 - coreweb GmbH
 * @author Christoph Vieth <cvieth@coreweb.de>
 * @version 1.2
 */
class ptbModuleLoader
{
    /**
     * List of registered modules
     * @var array
     */
    private $moduleList = array();

    /**
     * Registers a given ptbModule
     *
     * @param   ptbModuleInstance $module
     * @throws  Exception
     */
    public function register(ptbModuleInstance $module)
    {
        if (!isset($this->moduleList[get_class($module)]))
            $this->moduleList[get_class($module)] = $module;
        else
            throw new Exception(LANG_ERROR_MODULE_ALREADY_LOADED);
    }

    /**
     * Checks if a given name of a ptbModule is already loaded
     *
     * @param   string $moduleName     Name of ptbModule
     * @return  boolean                     Returns true if module exists else false
     */
    public function exists($moduleName)
    {
        if (isset($this->moduleList[$moduleName]))
            return true;
        else
            return false;
    }

    /**
     * Runs a given module
     *
     * @param   string $moduleName name of ptbModule to execute
     * @return  void
     * @throws Exception
     */
    public function run($moduleName)
    {
        if (isset($this->moduleList[$moduleName]))
            return $this->moduleList[$moduleName]->run();
        else
            throw new Exception(LANG_ERROR_MODULE_NOT_LOADED);
    }

}