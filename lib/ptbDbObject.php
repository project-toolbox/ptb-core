<?php
/**
 * Project Toolbox - ptbDbObject
 *
 * Library vor database management
 *
 * @copyright coreweb GmbH
 * @author Christoph Vieth <cvieth@coreweb.de>
 * @version 1.2
 */

class ptbDbObject
{
    private $dbConnections = array();

    /**
     * Constructor of ptbDbObject
     *
     * @param $dbConfigs
     * @throws Exception
     */
    public function __construct($dbConfigs)
    {
        if (is_array($dbConfigs)) {
            foreach ($dbConfigs as $dbConfig) {
                /**
                 * @var $dbConfig ptbDbConfig
                 */
                if (is_object($dbConfig) && (get_class($dbConfig) == 'ptbDbConfig')) {
                    $this->loadConfig($dbConfig);
                } else
                    throw new Exception(LANG_ERROR_DB_CONF_NOT_OBJECT);
            }
        } else
            throw new Exception(LANG_ERROR_DB_CONF_NOT_ARRAY);

    }

    /**
     * Load database configuration
     *
     * @param ptbDbConfig $dbConfig
     * @return bool
     */
    private function loadConfig(ptbDbConfig $dbConfig)
    {
        try {
            $this->dbConnections[$dbConfig->getDbName()] = new PDO('mysql:dbname=' . $dbConfig->getDbSchema() . ';host=' . $dbConfig->getDbHost() . ';port=' . $dbConfig->getDbPort(), $dbConfig->getDbUser(), $dbConfig->getDbPass(), array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
        } catch (PDOException $e) {
            die(LANG_ERROR_DB_MESSAGE . $e->getMessage());
        }
        return true;
    }

    /**
     * Recive a database connection from ptbDbObject
     *
     * @param $name
     * @return bool
     */
    public function getConnection($name)
    {
        return (isset($this->dbConnections[$name])) ? $this->dbConnections[$name] : false;
    }

}

/**
 * Create object instance
 */
require ptbCoreConfig::pathBase . ptbCoreConfig::pathConfigs . 'ptbDatabases.php';
$ptbDbObject = new ptbDbObject($ptbDatabases);